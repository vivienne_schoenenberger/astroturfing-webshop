import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import "./Navbar.css"
import Logo from './nomineAstraLogo.png'


function Navbar() {
    return(
        <div>
            <img className='logo' src={Logo}></img>
            <FavoriteBorderIcon className='favourite' fontSize='large'></FavoriteBorderIcon>
            <ShoppingCartIcon className='cart' fontSize='large'></ShoppingCartIcon>
            <AccountCircleIcon className='account' fontSize='large'></AccountCircleIcon>
        </div>
    )
}

export default Navbar;