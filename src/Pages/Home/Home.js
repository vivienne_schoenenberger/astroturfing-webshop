import './Home.css'
import Navbar from '../../Components/Navbar/Navbar';
import Offer from '../../Components/Offer/Offer';
import SearchIcon from '@mui/icons-material/Search';
import { Form, Container, Row, Col, Button } from 'react-bootstrap';

function Home() {
    return(
        <div className='home'>
            <Navbar></Navbar>

            <div>
                <Container className='search'>
                    <Row>
                        <Col>
                            <Form.Control className='form' type="text"/>
                        </Col>
                        <Col>
                            <Button className='search-button'><SearchIcon></SearchIcon></Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col className='col-offer'>
                            <Offer></Offer>
                        </Col>
                    </Row>
                </Container>
            
            </div>
            
        </div>
    )
}

export default Home;